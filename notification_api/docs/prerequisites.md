Notification_api newsletter service.
Documentation is available at(127.0.0.1, 0.0.0.0, 127.0.0.1/doc, 0.0.0.0/doc),
1.Every 24 hours, information is sent to clients whose email addresses are in the database
2.Every 24 hours a mailing check is made, if a mailing is created with a date in the past, the task is sent to tasks and executed.
There are two ways to start:
To run without a docker:

DATABAs
1. python manage.py runserver
2. python -m celery -A notification_api worker -l info -P gevent
3. python -m celery -A notification_api beat -l info

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.getenv("DB_NAME"),
        'USER': os.getenv("DB_USER"),
        'PASSWORD': os.getenv("DB_PASS"),
        'HOST': os.getenv("DB_HOST"), # db
    }
}

To run with a docker:
1. docker-compose down -v
2. docker-compose build
3. docker-compose up

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.getenv("DB_NAME"),
        'USER': os.getenv("DB_USER"),
        'PASSWORD': os.getenv("DB_PASS"),
        'HOST': os.getenv("DB_HOST"), # localhost
        'PORT': 5432,
    }
}