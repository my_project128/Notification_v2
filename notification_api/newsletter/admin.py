from django.contrib import admin

from .models import Client, Message, Newsletter


class ClientAdmin(admin.ModelAdmin):
    list_display = ('phone', )
    search_fields = ['id', 'email', 'data_created', 'operator_code']


class MessageAdmin(admin.ModelAdmin):
    list_display = ('text', )
    search_fields = ['id', 'title', 'data_Created']

class NewsletterAdmin(admin.ModelAdmin):
    list_display = ('title', )
    search_fields = ['id', 'title', 'data_Created']
    list_select_related = ('client', 'message', )


admin.site.register(Client, ClientAdmin)
admin.site.register(Message, MessageAdmin)
admin.site.register(Newsletter, NewsletterAdmin)
