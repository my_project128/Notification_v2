from django.db import models


class Client(models.Model):
    email = models.EmailField(unique=True)
    phone = models.CharField(max_length=100, unique=True)
    operator_code = models.CharField(max_length=5)

    date_created = models.DateTimeField(auto_now_add=True)


class Message(models.Model):
    STATUS_SEND = (
        ('S', 'Sent'),
        ('Uns', 'Unsent')
    )

    title = models.CharField(max_length=100)
    text = models.TextField(null=True)
    sent_status = models.CharField(max_length=7, choices=STATUS_SEND)

    data_created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title

class Newsletter(models.Model):
    title = models.CharField(max_length=100)

    date_start = models.DateTimeField()
    date_stop = models.DateTimeField()

    message = models.ForeignKey(Message, on_delete=models.CASCADE)
    client = models.ForeignKey(Client, on_delete=models.SET_NULL,  null=True, default=None, blank=True)

    def __str__(self):
        return self.title


class Statistics(models.Model):
    sent_message = models.TextField()

    data_created = models.DateTimeField(auto_created=True)
