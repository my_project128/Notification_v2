from rest_framework import serializers

from .models import Client, Message, Newsletter


class Clientserializers(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = '__all__'

    def validate(self, data):
        phone = data.get('phone', None)
        operator_code = data.get('operator_code', None)

        if 10 >= len(phone.replace(operator_code, '')) >= 7:
            return data
        else:
            raise serializers.ValidationError('The phone number is incorrect')

    def create(self, validated_data):
        return Client.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.email = validated_data.get("email", instance.email)
        instance.phone = validated_data.get("phone", instance.phone)
        instance.operator_code = validated_data.get("operator_code", instance.operator_code)

        instance.save()

        return instance


class MessageSerialezers(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = '__all__'

    def create(self, validated_data):
        return Message.objects.create(**validated_data)


class NewsletterSerializers(serializers.ModelSerializer):
    class Meta:
        model = Newsletter
        fields = '__all__'

    def create(self, validated_data):
        return Newsletter.objects.create(**validated_data)
