import os
import json
import requests

from celery import shared_task

from django.conf import settings
from django.core.mail import send_mail
from django.db.models import F


from notification_api.celery import app

from datetime import datetime

from .request_auth import BearerAuth
from .models import Client, Message, Newsletter, Statistics


def adding_information_to_statistics(added_text: str):
    text = Statistics.object.filter(date_created=datetime.now()).values('text')[0].get('text')
    text += f'//{added_text}//'

    Statistics.object.filter(date_created=datetime.now()).update(text=text)


def create_statistics(data):
    if Statistics.object.filter(date_created=datetime.now()) is not None:
        adding_information_to_statistics(data)
    else:
        Statistics.objects.create()
        adding_information_to_statistics(data)


def link_to_external_API(data):
    response = requests.post('https://probe.fbrq.cloud/v1/send/', auth=BearerAuth(os.environ['BEARER_TOKEN']), data=json.dumps(data))

    if response.status_code == 200:
        return response
    else:
        return None


@app.task
def sending_email_info_for_user():
    email_from = settings.EMAIL_HOST_USER
    data = Client.objects.prefetch_related('newsletter_set').values('email', 'phone', title=F('newsletter__title'))

    for client in data:
        client_email = client['email']
        if client['title'] is not None:
            subj = "Information about your personal account"
            message = f'You are subscribed to the following newsletters: {client["title"]}' \
                      f'You phone: {client["phone"]} ' \
                      f'Have a nice day!!!'

            send_mail(subj, message, email_from, [client_email, ])

        else:
            subj = "Information about your personal account"
            message = f'Sign up for newsletters and get useful information at any time!!!' \
                      f'Have a nice day!!!'

            send_mail(subj, message, email_from, [client_email, ])


@app.task
def order_create_for_phone_API():
    newsletters = Newsletter.objects.all()

    for newsletter_date in newsletters.values('date_start', 'date_stop'):
        if newsletter_date['date_start'] <= datetime.now() and newsletters['data_stop'] >= datetime.now():
            newsletter_info = Newsletter.objects.filter(date_start=newsletter_date['date_start']).values('id', 'message_id', 'client_id')

            client_id = newsletter_info['client_id']
            message_id = newsletter_info['message_id']

            Message.objects.filter(id=message_id).update(sent_status='Sent')
            message = Message.objects.filter(id=message_id).values('text')
            client_phone = ''.join([[i for i in v.values()] for v in Client.objects.filter(id=1).values('operator_code', 'phone')][0])

            result = {
                "id": newsletter_info["id"],
                "phone": client_phone,
                "text": message
            }
            if link_to_external_API(result) is not None:
                create_statistics(result)


@shared_task
def order_create_now(data):
    if data['date_started'] <= datetime.now():
        client_id = data['client_id']
        message_id = data['message_id']

        message = Message.objects.filter(id=message_id).values('text')
        client_phone = ''.join([[i for i in v.values()] for v in Client.objects.filter(id=1).values('operator_code', 'phone')][0])

        result = {
            "id": data["id"],
            "phone": client_phone,
            "text": message
        }

        if link_to_external_API(result) is not None:
            create_statistics(result)

    else:
        return None
