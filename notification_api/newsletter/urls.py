from django.urls import path
from .views import ClientView, MessageView, AllClientView, NewsletterView


urlpatterns = [
    path('', NewsletterView.as_view()),
    path('client/', AllClientView.as_view()),
    path('client/change/<int:pk>/', ClientView.as_view()),
    path('message/', MessageView.as_view())
]
