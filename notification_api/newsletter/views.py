from drf_yasg.utils import swagger_auto_schema

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import AllowAny

from .models import Client, Message, Newsletter
from .serializers import Clientserializers, MessageSerialezers, NewsletterSerializers
from .tasks import order_create_now


class AllClientView(APIView):
    """Allows you to create a user and get information about all users"""
    permission_classes = (AllowAny,)
    serializers_class = Clientserializers

    def get(self, request):
        all_client = Client.objects.all()
        serializers = self.serializers_class(all_client, many=True)

        return Response({'all_clients': serializers.data}, status=200)

    @swagger_auto_schema(
        request_body=Clientserializers,
        request_method='POST',
        responses={
            200: Clientserializers
        }
    )

    def post (self, request):
        serializer = self.serializers_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response({'create_client': serializer.data}, status=200)


class ClientView(APIView):
    """Allows you to retrieve, modify and delete client data """
    permission_classes = (AllowAny, )
    serializers_class = Clientserializers

    def _client_search_by_pk(self, pk):
        if not pk:
            return Response({'error': 'method not allowed'})

        try:
            instance = Client.objects.get(pk=pk)
        except Client.DoesNotExist:
            return Response({"error": "object does not exists"})

        return instance

    def get(self, request, *args, **kwargs):
        pk = kwargs.get("pk", None)

        client = self._client_search_by_pk(pk)
        serializer = self.serializers_class(client)

        return Response({'client': serializer.data}, status=200)

    def delete(self, request, *args, **kwargs):
        pk = kwargs.get("pk", None)

        client = self._client_search_by_pk(pk)
        client.delete()

        return Response({'delete_client(id)': pk}, status=200)

    def put(self, request, *args, **kwargs):
        pk = kwargs.get("pk", None)

        serializer = self.serializers_class(instance=self._client_search_by_pk(pk), data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response({"update": serializer.data})


class MessageView(APIView):
    """Allows you to output and create a message to be sent later"""
    permission_classes = (AllowAny,)
    serializers_class = MessageSerialezers

    def get(self, request):
        all_message = Message.objects.all()
        serializer = self.serializers_class(all_message, many=True)

        return Response({'all_message': serializer.data}, status=200)

    @swagger_auto_schema(
        request_body=MessageSerialezers,
        request_method='POST',
        responses={
            200: MessageSerialezers
        }
    )

    def post(self, request):
        serializer = self.serializers_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response({'create_message': serializer.data}, status=200)


class NewsletterView(APIView):
    """Allows you to receive all available newsletters, to create a newsletter"""
    permission_classes = (AllowAny,) # IsAdminUser
    serializers_class = NewsletterSerializers

    def get(self, request):
        all_newsletter = Newsletter.objects.all()
        serializer = self.serializers_class(all_newsletter, many=True)

        return Response({'all_newsletter': serializer.data}, status=200)

    @swagger_auto_schema(
        request_body=NewsletterSerializers,
        request_method='POST',
        responses={
            200: NewsletterSerializers
        }
    )

    def post(self, request):
        serializer = self.serializers_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        order_create_now.delay(request.data)

        return Response({'create_newsletter': serializer.data}, status=200)
