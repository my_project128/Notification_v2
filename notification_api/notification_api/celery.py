from __future__ import absolute_import, unicode_literals

import os

from django.conf import settings

from celery import Celery
from celery.schedules import crontab


os.environ.setdefault("DJANGO_SETTINGS_MODULE", 'notification_api.settings')
BASE_REDIS_URL = os.environ.get('REDIS_URL', settings.CELERY_BROKER_URL)

app = Celery('notification_api')
app.config_from_object('django.conf:settings', namespace="CELERY")
app.autodiscover_tasks()
app.conf.broker_url = BASE_REDIS_URL

app.conf.beat_schedule = {
    'every': {
        'task': 'newsletter.tasks.sending_email_info_for_user',
        'schedule': crontab(hour=23),
    },
    'phone': {
        'task': 'newsletter.tasks.order_create_for_phone_API',
        'schedule': crontab(hour=23),
    }
}
